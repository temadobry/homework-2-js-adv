"use strict"

const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];

class NotAllValuesError extends Error {
    constructor() {
        super();
        this.name = "NotAllValuesError";
        this.message = "Not full information about book"
    }
}

class BookList {
    constructor(item) {
        if (Object.keys(item).length < 3) {
            throw new NotAllValuesError(item);
        }
        this.item = item;
    }

    render(container) {
        container.insertAdjacentHTML("beforeend", `
        <ul>
            <li>${Object.values(this.item)}</li>
        </ul>
        `)
    }

}

const container = document.querySelector("#root");

books.forEach((el) => {
    try {
        new BookList(el).render(container)
    }
    catch (err) {
        if (err.name === "NotAllValuesError") {
            console.warn(err)
        }
        else{
            throw err
        }
    }
    }
)

// const showList = (arr, container = document.body) => {
//     arr.forEach(item => container.insertAdjacentHTML("beforeend", `<li></li>`))
// }
//
// showList(books, container)